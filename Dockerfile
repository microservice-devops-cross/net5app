FROM mcr.microsoft.com/dotnet/aspnet:5.0-alpine

WORKDIR /usr/src/net5app

COPY ./bin/Release/net5.0/publish ./

EXPOSE 80

ENTRYPOINT [ "dotnet","net5app.dll" ]